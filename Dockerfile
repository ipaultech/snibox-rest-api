#working with golang:1.20.2-bullseye
FROM golang:bullseye

ARG DB_NAME
ARG DB_PASS
ARG DB_USER
ARG DB_HOST
ARG DB_PORT

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN go build -o /main

#API port
EXPOSE 8080
#Prometheus exporter port
EXPOSE 8081

CMD [ "/main" ]
