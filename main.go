package main

import (
	"net/http"
    "github.com/gin-gonic/gin"
	"database/sql"
    "fmt" 
    _ "github.com/lib/pq"
	"bufio"
	"os"
	"strings"
	"github.com/penglongli/gin-metrics/ginmetrics"
	"strconv"
)

//Init global snipfiles array with slice type SnippetFile
var snipfiles []SnippetFile

//Define struct for snippets
type Snippets struct {
	Label_Name string `json:"label_name"`
    Title     string  `json:"title"`
    Description string  `json:"description"`
	Id  int  `json:"id"`
	Label_Id int `json:"label_id"`
	Snippets_Count int `json:"snippets_count"`
	SnippetFiles []SnippetFile `json:"snippetFiles"`
}

//Define struct for snippetfiles (inside snippets)
type SnippetFile struct {
    Title     string  `json:"title"`
    Id  int  `json:"id"`
    Content string  `json:"content"`
	Language string `json:"language"`
	Created_at string `json:"created_at"`
	Updated_at string `json:"updated_at"`
	Snippet_id int `json:"snippet_id"`
}

//Message Print function
func printMessage(message string) {
    fmt.Println("")
    fmt.Println(message)
    fmt.Println("")
}

//DB Setup function
func setupDB() *sql.DB {
	//DB Vars
	DB_USER := os.Getenv("DB_USER")
	DB_PASSWORD := os.Getenv("DB_PASS")
	DB_NAME := os.Getenv("DB_NAME")
	DB_HOST := os.Getenv("DB_HOST")
	//Get port env var and convert it to a integer
	DB_PORT_STRING := os.Getenv("DB_PORT")
	DB_PORT, err := strconv.Atoi(DB_PORT_STRING)
	//Error check for int convert
	CheckError(err)

	//Init DB connection
    dbinfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME)
    db, err := sql.Open("postgres", dbinfo)
	//Check for error
    CheckError(err)

    return db
}

//Main function
func main() {
	//Gin init
	router := gin.Default()

	// Metrics handling for prometheus
	// Thanks to: https://github.com/penglongli/gin-metrics
	metricRouter := gin.Default()

	// get global Monitor object
	m := ginmetrics.GetMonitor()
	// use metric middleware without expose metric path
	m.UseWithoutExposingEndpoint(router)
	// +optional set metric path, default /debug/metrics
	m.SetMetricPath("/metrics")
	// +optional set slow time, default 5s
	m.SetSlowTime(10)
	// set metric path expose to metric router
	m.Expose(metricRouter)
	go func() {
		_ = metricRouter.Run(":8081")
	}()

	// User auth handling from plaintext file
	users := make(gin.Accounts)
	file, err := os.Open("auth_users.txt")
	CheckError(err)
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		user := strings.Split(line, ":")
		users[user[0]] = user[1]
	}
	//Check for errors
	CheckError(err)

	// Create Sub Router for customized API version and basic auth
	subRouterAuthenticated := router.Group("/api/v1/get/snippets/all", gin.BasicAuth(users))
	subRouterAuthenticated.GET("/", getAllSnippets)

	//Run it!
    router.Run(":8080")
}
 
//Error Checker
func CheckError(err error) {
    if err != nil {
        panic(err)
    }
}

//Create SnippetFile slice array from db query
func getSnippetFiles() {
	db := setupDB()
	//Get all snippet files
	snippetfilesrows, err := db.Query("SELECT f.title, f.id, f.content, f.language, f.created_at, f.updated_at, f.snippet_id FROM snippet_files AS f ORDER BY f.snippet_id;")
	//Check for errors
	CheckError(err)
	//Close DB stuff
	defer snippetfilesrows.Close()

	//Clear old snippetfiles
	snipfiles = nil

	//Get all snipet files to an array
	for snippetfilesrows.Next() {
		//Set vars
		var title string
		var id int
		var content string
		var language string
		var created_at string
		var updated_at string
		var snippet_id int

		err = snippetfilesrows.Scan(&title, &id, &content, &language, &created_at, &updated_at, &snippet_id)
		// check errors
		CheckError(err)
		//Add append to array
		snipfiles = append(snipfiles, SnippetFile{Title: title, Id: id, Content: content, Language: language, Created_at: created_at, Updated_at: updated_at, Snippet_id: snippet_id})
	}
}

// Get all snippets with its snippetfiles
func getAllSnippets(c *gin.Context) {
	//Init DB
	db := setupDB()

	//Get all snippets from db
	rows, err := db.Query("SELECT l.name as label_name , s.title, s.description, s.id, s.label_id, s.snippet_files_count FROM snippets AS s INNER JOIN labels AS l ON l.id = s.label_id;")
	//check for errors
	CheckError(err)
	//Close DB stuff
	defer rows.Close()

	//Get snippet files from db
	getSnippetFiles()
	//init snippet array
	var snippets []Snippets

	// Foreach movie
	for rows.Next() {
		//Set vars
		var label_name string
		var title string
		var description string
		var id int
		var label_id int
		var snippet_files_count int

		err = rows.Scan(&label_name, &title, &description, &id, &label_id, &snippet_files_count)
		//check errors
		CheckError(err)

		//Init assigned snippetfile array from Snippet File array
		var assigned_snippets []SnippetFile
		for _, e := range snipfiles {
			if id == e.Snippet_id {
				assigned_snippets = append(assigned_snippets, SnippetFile{Title: e.Title, Id: e.Id, Content: e.Content, Language: e.Language, Created_at: e.Created_at, Updated_at: e.Updated_at, Snippet_id: e.Snippet_id})
			}
		}

		//Apend all to snippets array
		snippets = append(snippets, Snippets{Label_Name: label_name, Title: title, Description: description, Id: id, Label_Id: label_id, Snippets_Count: snippet_files_count, SnippetFiles: assigned_snippets})
	}
	
	CheckError(err)
    c.IndentedJSON(http.StatusOK, snippets)
}