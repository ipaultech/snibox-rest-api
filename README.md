# Snibox Rest API

I've created this project because [Snibox](https://github.com/snibox/snibox#demo) has no API that does what i want!

## Preview
![image](/demo.png)

## ⚙️ Features
- Export all snippets (to JSON)
- Prometheus metrics
- Multiple users (basic auth, until yet..)

## Deployment

The best way to deploy is ``docker compose``.
See the example file.

The API listens on this URL:
``http://yourhost:8080/api/v1/get/snippets/all/``

The metric exporter for prometheus listens on:
``http://yourhost:8081/metrics``

